var app = angular.module('maaApp',['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

    $stateProvider
    .state('index',
    {
      url: "/",
      views: {
          "content": {templateUrl: "/content/index/index.html"},
          "sidebar": {templateUrl: "/content/index/indexaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('theagency',
    {
      url: "/about/theagency",
      views: {
          "content": {templateUrl: "/content/about/theagency.html"},
          "sidebar": {templateUrl: "/content/about/aboutaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('missionstatement',
    {
      url: "/about/missionstatement",
      views: {
          "content": {templateUrl: "/content/about/missionstatement.html"},
          "sidebar": {templateUrl: "/content/about/aboutaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('testimonials',
    {
      url: "/about/testimonials",
      views: {
          "content": {templateUrl: "/content/about/testimonials.html"},
          "sidebar": {templateUrl: "/content/about/aboutaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('auto',
    {
      url: "/auto",
      views: {
          "content": {templateUrl: "/content/auto/personal.html"},
          "sidebar": {templateUrl: "/content/auto/autoaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('personal',
    {
      url: "/auto/personal",
      views: {
          "content": {templateUrl: "/content/auto/personal.html"},
          "sidebar": {templateUrl: "/content/auto/autoaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('commercial',
    {
      url: "/auto/commercial",
      views: {
          "content": {templateUrl: "/content/auto/commercial.html"},
          "sidebar": {templateUrl: "/content/auto/autoaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('home',
    {
      url: "/home",
      views: {
          "content": {templateUrl: "/content/home/homeowners.html"},
          "sidebar": {templateUrl: "/content/home/homeaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('condo',
    {
      url: "/condo",
      views: {
          "content": {templateUrl: "/content/home/condo.html"},
          "sidebar": {templateUrl: "/content/home/homeaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('renters',
    {
      url: "/renters",
      views: {
          "content": {templateUrl: "/content/home/renters.html"},
          "sidebar": {templateUrl: "/content/home/homeaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('vacantstructures',
    {
      url: "/vacantstructures",
      views: {
          "content": {templateUrl: "/content/home/vacantstructures.html"},
          "sidebar": {templateUrl: "/content/home/homeaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('business',
    {
      url: "/business",
      views: {
          "content": {templateUrl: "/content/business/generalliability.html"},
          "sidebar": {templateUrl: "/content/business/businessaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('contractorequipment',
    {
      url: "/business/contractorequipment",
      views: {
          "content": {templateUrl: "/content/business/contratorequipment.html"},
          "sidebar": {templateUrl: "/content/business/businessaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('workerscompensation',
    {
      url: "/business/workerscompensation",
      views: {
          "content": {templateUrl: "/content/business/workerscompensation.html"},
          "sidebar": {templateUrl: "/content/business/businessaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('bond',
    {
      url: "/business/bond",
      views: {
          "content": {templateUrl: "/content/business/bond.html"},
          "sidebar": {templateUrl: "/content/business/businessaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('buildersrisk',
    {
      url: "/business/buildersrisk",
      views: {
          "content": {templateUrl: "/content/business/buildersrisk.html"},
          "sidebar": {templateUrl: "/content/business/businessaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('lifeinsurance',
    {
      url: "/lifeinsurance",
      views: {
          "content": {templateUrl: "/content/lifeinsurance/termlife.html"},
          "sidebar": {templateUrl: "/content/lifeinsurance/lifeinsuranceaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('wholelife',
    {
      url: "/wholelife",
      views: {
          "content": {templateUrl: "/content/lifeinsurance/wholelife.html"},
          "sidebar": {templateUrl: "/content/lifeinsurance/lifeinsuranceaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('health',
    {
      url: "/health",
      views: {
          "content": {templateUrl: "/content/health/individualcoverage.html"},
          "sidebar": {templateUrl: "/content/health/healthaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('hsaplan',
    {
      url: "/health/hsaplan",
      views: {
          "content": {templateUrl: "/content/health/hsaplan.html"},
          "sidebar": {templateUrl: "/content/health/healthaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('dentalandvision',
    {
      url: "/health/dentalandvision",
      views: {
          "content": {templateUrl: "/content/health/dentalandvision.html"},
          "sidebar": {templateUrl: "/content/health/healthaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('disability',
    {
      url: "/disability",
      views: {
          "content": {templateUrl: "/content/disability/index.html"},
          "sidebar": {templateUrl: "/content/disability/disabilityaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('services',
    {
      url: "/services",
      views: {
          "content": {templateUrl: "/content/services/index.html"},
          "sidebar": {templateUrl: "/content/services/servicesaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('contact',
    {
      url: "/contact",
      views: {
          "content": {templateUrl: "/content/contact/index.html"},
          "sidebar": {templateUrl: "/content/contact/contactaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('customerservice',
    {
      url: "/customerservice",
      views: {
          "content": {templateUrl: "/content/customerservice/index.html"},
          "sidebar": {templateUrl: "/content/customerservice/customerserviceaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    })
    .state('requestaquote',
    {
      url: "/requestaquote",
      views: {
          "content": {templateUrl: "/content/quotes/index.html"},
          "sidebar": {templateUrl: "/content/quotes/quotesaside.html"},
          // default content
          "parnerships": {templateUrl: "/content/partnerships.html"},
          "search":  {templateUrl: "/content/search.html"},
          "footer": {templateUrl: "/content/footer.html"},
          "copyright": {templateUrl: "/content/copyright.html"}
      }
    });
});

app.controller('headerbarController', ['$scope', function($scope){
  // Phone Number for socialbar
  $scope.phoneIcon = "fa fa-phone";
  $scope.phoneNumber = "1-877-491-5078";
  // Email Address for socialbar
  $scope.emailIcon = "fa fa-envelope";
  $scope.emailAddress = "info@maa-insurance.com";
}]);

// socialController displays the socialmedia icons, phone and email in bar along top of page.
app.controller('socialbarController', ['$scope','$http', function($scope, $http){
  // Get the social media icons and properties from a json file
  $http.get('json-data/socialmedia.json').success(function(data){
    $scope.socialmedia = data;
  });

}]);

app.controller('homeController',['$scope', function($scope){
  $scope.message = "Hello Home";
}]);

app.controller('aboutController',['$scope', function($scope){
  $scope.message = "Hello About";
}]);

app.controller('marketPlaceController' ,['$scope', function($scope){

}]);
