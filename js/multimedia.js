$("document").ready(function(){
  var i = 1;
  var campaigns = [
    {
      src: "img/campaigns/are_you_covered.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "How long could you afford to be without a paycheck?",
      hdcolor: "red",
      teasertext: "Disability Income Insurance works when you can't.",
      teasercolor: "#ffff01",
      campaignbuttontext: "Get Covered",
      campaignurl: "",
      bgcolor: "#e2a467"
    },
    {
      src: "img/campaigns/3_reasons.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "Along with valuable <br />and affordable products",
      hdcolor: "#ffbb04",
      teasertext: "Here's 3 more good reason's to switch to <br />Montgomery Agency & Associates today.",
      teasercolor: "#ffffff",
      campaignbuttontext: "Learn more...",
      campaignurl: "",
      bgcolor: "#404040"
    },
    {
      src: "img/campaigns/auto.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "Enjoy your ride  <br />with peace of mind",
      hdcolor: "#2b5da0",
      teasertext: "Like us on our FB page and take the <b>#InsuranceChallenge</b>.",
      teasercolor: "#000",
      campaignbuttontext: "Get a Quote Now!",
      campaignurl: "",
      bgcolor: ""
    },
    {
      src: "img/campaigns/construction.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "Focus on growing <br /> your Business",
      hdcolor: "#ffbb04",
      teasertext: "<b>We'll focus on protecting it...</b>",
      teasercolor: "#fff",
      campaignbuttontext: "Get a Quote Now!",
      campaignurl: "",
      bgcolor: "#c6e2f8"
    },
    {
      src: "img/campaigns/family.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "Affordable Health Plans for Individuals and Families.",
      hdcolor: "#02a5f1",
      teasertext: "Dental Plans with optional Vision benefit, <br />Supplemental Accident benefit...",
      teasercolor: "#21219b",
      campaignbuttontext: "Learn more...",
      campaignurl: "",
      bgcolor: "#e0e8eb"
    },
    {
      src: "img/campaigns/got_insurance.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "You've worked doggone <br />hard for your life possesions",
      hdcolor: "#e22c28",
      teasertext: "<b>Protect them now with:</b> <i>Homeowner, Renters, Condo, or Mobile Home Insurance</i>.",
      teasercolor: "#000",
      campaignbuttontext: "Get Protection",
      campaignurl: "",
      bgcolor: ""
    },
    {
      src: "img/campaigns/life_insurance.jpg",
      fadetime: 3000,
      slideduration: 10000,
      hdtext: "Life happens, <br />enjoy the journey...",
      hdcolor: "#769e84",
      teasertext: "Protect your loved ones, Secure your Final Expenses and Build Wealth.",
      teasercolor: "#000",
      campaignbuttontext: "Learn More...",
      campaignurl: "",
      bgcolor: "#e8e4d8"
    }
  ];

  $('#marketplace').css(
    {
      "background-image":"url("+ campaigns[0].src +")",
      "display":"none",
      "background-color": campaigns[0].bgcolor
    }
  ).fadeIn(campaigns[0].fadetime);

  $('#campaignheader').css({
    "color": campaigns[0].hdcolor,
    "font-size": "30pt",
    "font-weight": 900,
    "line-height": "92%"
  }).html(campaigns[0].hdtext);

  $('#teasertext').css({
    "color": campaigns[0].teasercolor,
    "font-size":"18pt",
    "font-weight": 200
  }).html(campaigns[0].teasertext);

  $('#campaignbutton').css({
    "float":"right"
  }).text(campaigns[0].campaignbuttontext)
  .on('click', function(){
    location.href = campaigns[0].campaignurl;
  });

  function campaign(){

    $('#marketplace').css(
      {
        "background-image":"url("+ campaigns[i].src +")",
        "display":"none",
        "background-color": campaigns[i].bgcolor
      }
    ).fadeIn(campaigns[i].fadetime);

    $('#campaignheader').css({
      "color": campaigns[i].hdcolor,
      "font-size": "30pt",
      "font-weight": 900,
      "line-height": "95%"
    }).html(campaigns[i].hdtext);

    $('#teasertext').css({
      "color": campaigns[i].teasercolor,
      "font-size":"18pt",
      "font-weight": 200
    }).html(campaigns[i].teasertext);

    $('#campaignbutton').css({
      "float":"right"
    })
    .text(campaigns[i].campaignbuttontext)
    .on('click', function(){
      location.href = campaigns[i].campaignurl;
    });

    if(i === campaigns.length -1)
    {
      i = 0;
    }else{
      i++;
    }
    //console.log(campaigns[i].src + " - Duration: " + campaigns[i].slideduration);
  }
  setInterval(campaign, campaigns[i].slideduration);

  $('h1').on('click', function(){
    alert("Sidebar Item was Hovered.");
  });

});
